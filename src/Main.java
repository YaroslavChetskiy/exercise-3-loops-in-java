import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        // через while
        int count = 1;
        int fib1 = 1;
        int fib2 = 1;
        while (count <= 11) {
            if (count == 1 || count == 2) {
                System.out.println(1);
            }
            else {
                int tmp = fib1;
                fib1 = fib2;
                fib2 = tmp + fib2;
                System.out.println(fib2);
            }
            count++;
        }
        System.out.println("----"); // разделитель вывода (для красоты)
        //через for
        fib1 = 1;
        fib2 = 1;
        for (int i = 1; i <= 11; i++) {
            if (i == 1 || i == 2) {
                System.out.println(1);
            }
            else {
                int tmp = fib1;
                fib1 = fib2;
                fib2 = tmp + fib2;
                System.out.println(fib2);
            }
        }
    }
}
